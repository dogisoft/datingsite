<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::auth();

Route::get('/home', 'HomeController@index');

Route::resource('/profile/step1', 'ProfileController@index');
Route::resource('/profile/step2', 'ProfileController@step2');
Route::resource('/profile/step3', 'ProfileController@step3');
Route::get('/support', 'HomeController@underconstruction');
Route::get('/messages', 'HomeController@underconstruction');
Route::get('/search', 'HomeController@underconstruction');
Route::get('/account', 'AccountController@index');
Route::resource('/account/update', 'AccountController@update');
Route::resource('/profile/save', 'ProfileController@save');
Route::resource('/profile/chkdate', 'ProfileController@chkdate');
Route::post('/profile/upload', 'FileController@upload');
