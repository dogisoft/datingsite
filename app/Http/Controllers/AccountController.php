<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use DB;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$message = "";
		$activeSuccess = "";
		$activeDanger = "";
		
		$data = $request->user();
        return view('account', ['user' => $data, 'message' => $message, 'activeSuccess' => $activeSuccess, 'activeDanger' => $activeDanger]);
    }
	
	public function update(Request $request){
		$user = $request->user();	
		$name = $request->input('name'); 
		$email = $request->input('email'); 
		$password = $request->input('password');
		$passwordConfirm = $request->input('password_confirmation');
		$message = "";
		$activeSuccess = "";
		$activeDanger = "";		
		
		if(!preg_match('^[-a-zA-Z \']*$^', $name)) {
			$message = 'Name is incorrect<br>';
		} 
		else{
			
			if($password==""){
				$user->name = $name;
				$user->email = $email;
				$user->save();
			}
			else{
				if($password == $passwordConfirm){
					
					$oldpw = $request->input('old-password');
					
					if($oldpw!==""){										
						$hashed = \Hash::make($oldpw);					
						if (\Hash::check($oldpw, $user->password)) {
							$user->password = \Hash::make($password);
							$user->save();
						}
						else{
							$activeDanger = 'Old password is not confirmed.';			
						}						
					}
					else{
						$activeDanger = "To change password please enter the current one.";
					}
				
				}					
			}		
		}			
		
		return view('account', ['user' => $user, 'message' => $message, 'activeSuccess' => $activeSuccess, 'activeDanger' => $activeDanger]);
	}
	
	
}
