<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $getTests = (new ProfileController)->countperc($request);

        if($getTests<50){
            return redirect()->intended('/profile/step1');
        }
        else{
            return view('home', ['perc' => $getTests]);
        }

    }
	
    public function underconstruction()
    {
        return view('underconstruction');
    }	
}
