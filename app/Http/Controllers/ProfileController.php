<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use DB;

class ProfileController extends Controller
{
    /**
     * Update the user's profile.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
		/** Display updated form */
		$data = $request->user();
		$profile = Profile::where('userID', $data->id)->get();
        
		return view('profile.profile', ['user' => $data, 'profile' => $profile, 'perc' => $this->countperc($request)]);		
    }
	
	public function step2(Request $request){
			/** Display updated form */
		$data = $request->user();
		$profile = Profile::where('userID', $data->id)->get();
		return view('profile.step2', ['user' => $data, 'profile' => $profile, 'perc' => $this->countperc($request)]);
	}
	
	public function step3(Request $request){
		$user = $request->user();
		$profile = Profile::where('userID', $user->id)->get();
		return view('profile.step3', ['user' => $user, 'profile' => $profile, 'perc' => $this->countperc($request)]);
	}	
	
	public function save(Request $request){
		
		$user = $request->user();
		$val = $request->input('value');
		$field = $request->input('field');
		
		if($field=="inputLicenseNum"){	$field = "license_num";		}	
		if($field=="scheduleType"){		$field = "schedule_type";	}	
		if($field=="jobType"){			$field = "job_type";		}	
		if($field=="inputMonth"){		$field = "bMonth";			}	
		if($field=="inputDay"){			$field = "bDay";			}	
		if($field=="inputYear"){		$field = "bYear";			}
		if($field=="inputCity"){		$field = "city";			}
		if($field=="inputSex"){			$field = "sex";				}		
		if($field=="inputState"){		$field = "state";			}
		if($field=="inputZip"){			$field = "zip";				}
		if($field=="photo"){			$field = "profilePic";		}

		if($field=="seeking"){			$field = "seeking";			}
		if($field=="religion"){			$field = "religion";		}
		if($field=="pets"){				$field = "pet";				}
		if($field=="smoke"){			$field = "smoke";			}
		if($field=="relstat"){			$field = "relationship_status";	}
		if($field=="children"){			$field = "children";		}
		if($field=="drink"){			$field = "drink";			}
		if($field=="about"){			$field = "about_you";		}
		if($field=="lookingfor"){		$field = "looking_for";		}
		if($field=="hobbies"){			$field = "hobbies_interest";}

		if($field=="free"){				$field = "package_type";	}	
		if($field=="silver"){			$field = "package_type";	}	
		if($field=="gold"){				$field = "package_type";	}	

		Profile::where('userID', $user->id)->update([$field => $val]);
	}	

	public function countperc(Request $request){
		$perc = 0;
		$user = $request->user();
		$profile = Profile::where('userID', $user->id)->first();

			if($profile->profilePic!==""){ $perc++;}
			if($profile->age!=="" && $profile->age>0){	$perc++;	}
			if($profile->sex!=="" && $profile->sex>0){	 $perc++;	}
			if($profile->bDay!=="" && $profile->bDay>0){	$perc++;	}
			if($profile->bMonth!=="" && $profile->bMonth>0){	$perc++;	}
			if($profile->bYear!=="" && $profile->bYear>0){	$perc++;	}
			if($profile->city!==""){	$perc++;	}
			if($profile->state!==""){	$perc++;	}
			if($profile->zip!==""){	$perc++;	}
			if($profile->license_num!==""){	$perc++;	}
			if($profile->schedule_type!==""){	$perc++;	}
			if($profile->job_type!==""){	$perc++;	}
			if($profile->seeking!==""){	$perc++;	}
			if($profile->relationship_status!==""){	$perc++;	}
			if($profile->religion!==""){	$perc++;	}
			if($profile->pet!==""){	$perc++;	}
			if($profile->smoke!==""){	$perc++;	}
			if($profile->children!==""){	$perc++;	}
			if($profile->drink!==""){	$perc++;	}
			if($profile->about_you!==""){	$perc++;	}
			if($profile->looking_for!==""){	$perc++;	}
			if($profile->hobbies_interest!==""){	$perc++;	}
			if($profile->package_type!==""){	$perc++;	}
		
		$perc = ($perc * 100)/27;
		return round($perc);
	}
	
	public function chkdate(Request $request){
		$year = $request->input("year");
		$month = $request->input("month");
		if($year){
			print cal_days_in_month(CAL_GREGORIAN, $month, $year);
		}
	}
	
}