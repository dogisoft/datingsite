<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Support\Facades\Input;
use App\Profile;
use DB;

class FileController extends Controller
{
    /**
     * Update the user's profile.
     *
     * @param  Request  $request
     * @return Response
     */
  

	public function upload(Request $request){				
		
		$destinationPath = "uploads";
		$fileName = "image_".time().".jpg";
		//$user = $request->user();
		
		if(Request::ajax()){	
			$user = Input::get('userid');		
			Input::file('photo')->move($destinationPath, $fileName);
			
				\DB::table('profile')
				->where('userID', $user)
				->update(['profilePic' => $fileName]);			
		}										

		
		
	}
	
}