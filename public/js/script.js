$(document).ready(function(){

	$('.register #name').on('focusout', function(){	
		var name = $("#name").val();
		if(name=="" || validateUsername($("#name").val())==false){			
			$("#error").html("Only letters, space, dash and apostrophe characters are allowed in the name.");		
		}	
	});
	
	$("#accountForm button").click(function(){
		var name = $("#name").val();
		if(name=="" || validateUsername($("#name").val())==false){			
			$(".alert-danger").fadeIn();
			$(".alert-danger #eMes").html("Only letters, space, dash and apostrophe characters are allowed in the name.");		
		}
		else{			
		
			if($("#email").val()===""){
				$(".alert-danger").fadeIn();
				$(".alert-danger #eMes").html("Email field is empty.");		
			}
			else{
	
				if($("#password").val()!==""){
					

					if($("#password-confirm").val()===""){
						$(".alert-danger").fadeIn();
						$(".alert-danger #eMes").html("Password confirmation field is empty.");		
					}
					else{
						
						if($("#password-confirm").val()!==$("#password").val()){
							$(".alert-danger").fadeIn();
							$(".alert-danger #eMes").html("Password confirmation is wrong.");		
						}
						else{							
							$("#accountForm").submit();																					
						}											
					}					
					
				}
				else{
					$("#accountForm").submit();
				}
					
			}
		}

		
	});
	
	
	if ($('#checkbox').prop('checked')) {
		$('#saveItems a button').removeAttr("disabled");	
	}		
	$('#checkbox').change(function(){
		
		if ($('#checkbox').prop('checked')) {
			$('#saveItems a button').removeAttr("disabled");
			saveProfile('agreementAccepted', 1);
		}			
		else{
			$('#saveItems a button').attr("disabled", "disabled");
			saveProfile('agreementAccepted', 0);
		}
	});
	

	if ($('#checkbox2').prop('checked')) {
		$('#showNext button').removeAttr("disabled");	
	}		
	$('#checkbox2').change(function(){
		
		if ($('#checkbox2').prop('checked')) {
			$('#showNext button').removeAttr("disabled");
			saveProfile('termsAccepted', 1);
		}			
		else{
			$('#showNext a button').attr("disabled", "disabled");
			saveProfile('termsAccepted', 0);			
		}
	});
	
});



function saveProfile(id, value){
		
	if(id == "photo"){
			  
			var form = document.forms.namedItem("imagForm"); // high importance!, here you need change "yourformname" with the name of your form
			var formdata = new FormData(form); // high importance!

			$.ajax({
				async: true,
				type: "POST",
				dataType: "html", // or html if you want...
				contentType: false, // high importance!
				url: 'http://localhost:8000/profile/upload', // you need change it.
				data: formdata, // high importance!
				processData: false, // high importance!
				success: function (data) {

					//do thing with data....
					console.log(data);

				},
				timeout: 10000
			}); 
		  
	}
	else{
	
		$.ajax({
		  method: "POST",
		  url: "http://localhost:8000/profile/save",
		  data: { field: id, value: value }
		})
		  .done(function( msg ) {
			console.log( "Data Saved: " + msg );
		  });	
	}
}

	/////  validation
	/////  check profile data
	
	$('.profile :input').on('focusout', function(){

		if(this.id=="inputLicenseNum"){
			if(this.value==""){
				$("#noty").html("<p class='error'>Please add you license number.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);			
			}
		}
		
		if(this.id=="inputZip"){
			if(this.value==""){
				$("#noty").html("<p class='error'>Please add you ZIP code.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				
				if(isValidUSZip(this.value)){
					$("#noty").html("Data has been saved");
					showNoty($(this).offset().top, $(this).offset().left, this.id);
					saveProfile(this.id, this.value);
				}
				else{
					$("#noty").html("<p class='error'>Invalid ZIP code</p>");
					showNoty($(this).offset().top, $(this).offset().left, this.id);
									
				}
			
			
			}
		}

		if(this.id=="inputState"){
			if(this.value==0){
				$("#noty").html("<p class='error'>Please select you state.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}

		if(this.id=="inputSex"){
			if(this.value==0){
				$("#noty").html("<p class='error'>Please select your gender.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}		
		
		if(this.id=="inputMonth"){
			
			if($("#inputYear").val()!==0){
				callDateCheck();
			}
			
			if(this.value==0){
				$("#noty").html("<p class='error'>Please select the month of your birth.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}		
		
		if(this.id=="inputYear"){
			
			if($("#inputMonth").val()!==0){
				callDateCheck();
			}
			
			if(this.value==0){
				$("#noty").html("<p class='error'>Please select the year of your birth</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}		
				
		if(this.id=="inputDay"){
			if(this.value==0){
				$("#noty").html("<p class='error'>Please select the day of your birth.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}

		if(this.id=="jobType"){
			if(this.value==0){
				$("#noty").html("<p class='error'>Please select your job type.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}

		if(this.id=="scheduleType"){
			if(this.value==0){
				$("#noty").html("<p class='error'>Please select your schedule type.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}

		if(this.id=="inputCity"){
			if(this.value==0){
				$("#noty").html("<p class='error'>Please add your city.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}

		if(this.id=="seeking"){
			if(this.value==0){
				$("#noty").html("<p class='error'>Please select your interest.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}	

		if(this.id=="religion"){
			if(this.value==0){
				$("#noty").html("<p class='error'>Please add your religion.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}	

		if(this.id=="pets"){
			if(this.value==0){
				$("#noty").html("<p class='error'>Please add your pets.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}	

		if(this.id=="smoke"){
			if(this.value==0){
				$("#noty").html("<p class='error'>Please choose from the list.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}

		if(this.id=="relationship_status"){
			if(this.value==0){
				$("#noty").html("<p class='error'>Please choose your relationship status.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}	

		if(this.id=="children"){
			if(this.value==0){
				$("#noty").html("<p class='error'>Please choose from the list.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}	

		if(this.id=="drink"){
			if(this.value==0){
				$("#noty").html("<p class='error'>Please choose from the list.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}

		if(this.id=="about"){
			if(this.value==""){
				$("#noty").html("<p class='error'>Please write a few words about yourself.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}		
		
		if(this.id=="lookingfor"){
			if(this.value==0){
				$("#noty").html("<p class='error'>Please tell us. What you are looking for.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}

		if(this.id=="hobbies"){
			if(this.value==0){
				$("#noty").html("<p class='error'>Please choose from the list.</p>");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
			}
			else{
				$("#noty").html("Data has been saved");
				showNoty($(this).offset().top, $(this).offset().left, this.id);
				saveProfile(this.id, this.value);	
			}
		}
		
		if(this.id=="photo"){
				saveProfile(this.id, this.value);	
		}
		
		if(this.id=="silver"){
				saveProfile(this.id, this.value);	
		}	

		if(this.id=="gold"){
				saveProfile(this.id, this.value);	
		}	

		if(this.id=="free"){
				saveProfile(this.id, this.value);	
		}			

	});



function showNoty(topDist, leftDist, id){
	leftDist = leftDist+10;
	$("#noty").css({'left': leftDist, 'top': topDist+25}).fadeIn(200).delay(4700).fadeOut(1200);
}

function isValidUSZip(sZip) {
   return /^\d{5}(-\d{4})?$/.test(sZip);
}

function validateUsername(username){	

	var patt = new RegExp("^[-a-zA-Z \']*$");
	return res = patt.test(username);
	
}

function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function callDateCheck(){
	var month = $("#inputMonth").val();
	var year = $("#inputYear").val();
	
	console.log(month+" "+year);
		$.ajax({
		  method: "POST",
		  url: "http://localhost:8000/profile/chkdate",
		  data: { month: month, year: year }
		})
		  .done(function( msg ) {
			
			var elem = "<option value='0'>Please select the day</option>";
			
			for(var i=1; i<=msg; i++){
				elem += "<option value='"+i+"'>"+i+"</option>";
			}
			$("#inputDay").removeAttr("disabled");
			$("#inputDay").html(elem);
			
		  });		
}