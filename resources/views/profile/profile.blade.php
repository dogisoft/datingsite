@extends('layouts.app')

@section('content')

<?php 
$sTArray = array(
	0 => "Please choose your Schedule Type",
	1 => "8hour day shifts",
	2 => "8hour night shifts",
	3 => "8hr rotation day/night",
	4 => "12hr day shifts",
	5 => "12hr night shifts",
	6 => "12hr rotation day/night",
	7 => "Other"
);

$jTArray = array(
	0 => "Please choose your Job type",
	1 => "Registered nurse",
	2 => "Licensed practical nurse",
	3 => "Clinical nurse specialist",
	4 => "Nurse practitioner",
	5 => "Nurse case manager",
	6 => "Intensive care unit registered nurse",
	7 => "Travel registered nurse",
	8 => "Staff nurse",
	9 => "Emergency room registered nurse",
	10 => "Labor & delivery registered nurse",
	11 => "Nurse supervisor",
	12 => "Dialysis registered nurse",
	13 => "Post-anesthesia care unit registered nurse",
	14 => "Other"
);

$gTArray = array(
	0 => "Please choose your gender",
	1 => "Male",
	2 => "Female",
	3 => "Other"
);

$statesArray = array(
	0 => "Please choose your state",
    'AL'=>'Alabama',
    'AK'=>'Alaska',
    'AZ'=>'Arizona',
    'AR'=>'Arkansas',
    'CA'=>'California',
    'CO'=>'Colorado',
    'CT'=>'Connecticut',
    'DE'=>'Delaware',
    'DC'=>'District of Columbia',
    'FL'=>'Florida',
    'GA'=>'Georgia',
    'HI'=>'Hawaii',
    'ID'=>'Idaho',
    'IL'=>'Illinois',
    'IN'=>'Indiana',
    'IA'=>'Iowa',
    'KS'=>'Kansas',
    'KY'=>'Kentucky',
    'LA'=>'Louisiana',
    'ME'=>'Maine',
    'MD'=>'Maryland',
    'MA'=>'Massachusetts',
    'MI'=>'Michigan',
    'MN'=>'Minnesota',
    'MS'=>'Mississippi',
    'MO'=>'Missouri',
    'MT'=>'Montana',
    'NE'=>'Nebraska',
    'NV'=>'Nevada',
    'NH'=>'New Hampshire',
    'NJ'=>'New Jersey',
    'NM'=>'New Mexico',
    'NY'=>'New York',
    'NC'=>'North Carolina',
    'ND'=>'North Dakota',
    'OH'=>'Ohio',
    'OK'=>'Oklahoma',
    'OR'=>'Oregon',
    'PA'=>'Pennsylvania',
    'RI'=>'Rhode Island',
    'SC'=>'South Carolina',
    'SD'=>'South Dakota',
    'TN'=>'Tennessee',
    'TX'=>'Texas',
    'UT'=>'Utah',
    'VT'=>'Vermont',
    'VA'=>'Virginia',
    'WA'=>'Washington',
    'WV'=>'West Virginia',
    'WI'=>'Wisconsin',
    'WY'=>'Wyoming',
);
 
?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Profile - Step 1 / <a href="/profile/step2">Step 2</a> / <a href="/profile/step3">Step 3</a>
						<div class="pull-right">{{$perc}}%</div>
					</div>
                        <div class="panel-body register">
								{{ csrf_field() }}
							
							  <div class="alert alert-success">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<strong>Success!</strong> <span id="sMes"></span>
							  </div>
							  
							  <div class="alert alert-danger">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<strong>Error!</strong> <span id="eMes"></span>
							  </div>                            
							
                            
							<div class="col-lg-6">
		
                                <h3>Professional info</h3>
                                <div class="form-group row">
                                    <label for="inputLicenseNum" class="col-sm-4 control-label">License Number</label>
                                    <div class="col-sm-6">
                                        <input maxlength="15" type="text" class="form-control" id="inputLicenseNum" name="inputLicenseNum" value="{{ $profile[0]->license_num }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="scheduleType" class="col-sm-4 control-label">Schedule type</label>
                                    <div class="col-sm-6">									
                                        <select class="form-control" id="scheduleType" name="scheduleType">										
											<?php 										
											foreach($sTArray as $kulcs => $ertek){
												if($profile[0]->schedule_type == $kulcs){
													print "<option value=".$kulcs." selected>".$ertek."</option>";
												}
												else{
													print "<option value=".$kulcs.">".$ertek."</option>";
												}												
											}												
											?>
                                        </select>										
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="jobType" class="col-sm-4 control-label">Job type</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" id="jobType" name="jobType">
											<?php 										
											foreach($jTArray as $kulcs => $ertek){
												if($profile[0]->job_type == $kulcs){
													print "<option value=".$kulcs." selected>".$ertek."</option>";
												}
												else{
													print "<option value=".$kulcs.">".$ertek."</option>";
												}												
											}												
											?>											
										</select>									
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-6">
                                <h3>Personal info</h3>

                                <div class="form-group row">
                                    <label for="photo" class="col-sm-4 control-label">Photo</label>
									<form class="form-horizontal" id="imagForm" enctype="multipart/form-data" method="post" action="{{ url('public/profile/upload') }}" autocomplete="off">
									
	
                                    <div class="col-sm-6">    
										<input type="file" class="form-control" id="photo" name="photo">
										<input type="hidden" name="_token" value="{{ csrf_token() }}" /> 
										<input type="hidden" name="userid" value="<?=$user->id?>" /> 	
									<?php 
									if($profile[0]->profilePic){
										print "<img src=/public/uploads/".$profile[0]->profilePic." />";
									}
									?>											
									</div>

									</form>
                                </div>

                                <div class="form-group row">
                                    <label for="inputAge" class="col-sm-4 control-label">Date of birth </label>
                                    <div class="col-sm-6">
											<?php
												$selected1 = true;
												$selected2 = true;
												$selected3 = true;
												$selected4 = true;
												$selected5 = true;
												$selected6 = true;
												$selected7 = true;
												$selected8 = true;
												$selected9 = true;
												$selected10 = true;
												$selected11 = true;
												$selected12 = true;
	
												if($profile[0]->bMonth==1){ $selected1 = " selected";	}
												if($profile[0]->bMonth==2){ $selected2 = " selected";	}
												if($profile[0]->bMonth==3){ $selected3 = " selected";	}
												if($profile[0]->bMonth==4){ $selected4 = " selected";	}
												if($profile[0]->bMonth==5){ $selected5 = " selected";	}
												if($profile[0]->bMonth==6){ $selected6 = " selected";	}
												if($profile[0]->bMonth==7){ $selected7 = " selected";	}
												if($profile[0]->bMonth==8){ $selected8 = " selected";	}
												if($profile[0]->bMonth==9){ $selected9 = " selected";	}
												if($profile[0]->bMonth==10){ $selected10 = " selected";	}
												if($profile[0]->bMonth==11){ $selected11 = " selected";	}
												if($profile[0]->bMonth==12){ $selected12 = " selected";	}
												
											?>									
                                        <select class="form-control" id="inputMonth" name="inputMonth">
											<option value="0">Please select the month</option>
											<option value="1"<?=$selected1?>>January</option>
											<option value="2"<?=$selected2?>>February</option>
											<option value="3"<?=$selected3?>>March</option>
											<option value="4"<?=$selected4?>>April</option>
											<option value="5"<?=$selected5?>>May</option>
											<option value="6"<?=$selected6?>>June</option>
											<option value="7"<?=$selected7?>>July</option>
											<option value="8"<?=$selected8?>>August</option>			
											<option value="9"<?=$selected9?>>September</option>
											<option value="10"<?=$selected10?>>October</option>											
											<option value="11"<?=$selected11?>>November</option>						
											<option value="12"<?=$selected12?>>December</option>

										</select>
                                    </div>
                                </div>							
								
                                <div class="form-group row">
                                    <label for="inputAge" class="col-sm-4 control-label"></label>
                                    <div class="col-sm-6">										

											<?php 
											if($profile[0]->bYear!==0 && $profile[0]->bDay!==0 && $profile[0]->bMonth!==0){
												echo '<select class="form-control" id="inputDay" name="inputDay">';
													//echo '<option value="0">Please select the day</option>';
											
													$numDays = cal_days_in_month(CAL_GREGORIAN, $profile[0]->bMonth, $profile[0]->bYear);
													$elem = "<option value='0'>Please select the day</option>";
													
													for($i=1; $i<=$numDays; $i++){
														if($profile[0]->bDay==$i){
															$elem .= "<option value='".$i."' selected>".$i."</option>";
														}
														else{
															$elem .= "<option value='".$i."'>".$i."</option>";
														}
													}
													print $elem;
													echo "</select>";
											}
											else{
												echo '<select class="form-control" id="inputDay" name="inputDay" disabled>';
												echo '<option value="0">Please select the day</option>';
												echo "</select>";												
											}
											?>
										
                                    </div>
                                </div>
								
                                <div class="form-group row">
                                    <label for="inputAge" class="col-sm-4 control-label"></label>
                                    <div class="col-sm-6">								
                                        <select class="form-control" id="inputYear" name="inputYear">
											<option value="0">Please select the year</option>
											<?php 			
											$years = 100;
											$recentYear = date('Y', time());
											$firstYear = $recentYear-18;											
											for($i = 0; $i<$years; $i++){	
												if($profile[0]->bYear == $firstYear){
													print "<option value=".$firstYear." selected>".$firstYear."</option>";
												}
												else{
													print "<option value=".$firstYear.">".$firstYear."</option>";
												}
												$firstYear--;
											}												
											?>														
										</select>										
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputSex" class="col-sm-4 control-label">Gender</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" id="inputSex" name="inputSex">
											<?php 										
											foreach($gTArray as $kulcs => $ertek){
												if($profile[0]->sex == $kulcs){
													print "<option value=".$kulcs." selected>".$ertek."</option>";
												}
												else{
													print "<option value=".$kulcs.">".$ertek."</option>";
												}												
											}												
											?>											
										</select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputCity" class="col-sm-4 control-label">City</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="inputCity" name="inputCity" value="{{ $profile[0]->city }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputState" class="col-sm-4 control-label">State</label>
                                    <div class="col-sm-6">                                         
										<select type="text" class="form-control" name="inputState" id="inputState">
											<?php 										
											foreach($statesArray as $kulcs => $ertek){
												if($profile[0]->state == $kulcs){
													print "<option value=".$kulcs." selected>".$ertek."</option>";
												}
												else{
													print "<option value=".$kulcs.">".$ertek."</option>";
												}												
											}												
											?>	
										</select>											
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputZip" class="col-sm-4 control-label">ZIP</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="inputZip" id="inputZip" value="{{ $profile[0]->zip }}">
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <h3>License Agreement</h3>
                            <div>        
									<p>I am a medical professional and the number provided is mine.</p>
									<?php 
									if($profile[0]->agreementAccepted==1){
										echo '<p><div id="agree"><input type="checkbox" name="checkbox" id="checkbox" checked="checked" /> Agree</div> </p> ';
									}
									else{
										echo '<p><div id="agree"><input type="checkbox" name="checkbox" id="checkbox" /> Agree</div> </p> ';
									}
									?>                                                                                         
                                    
								<div id="saveItems">
									<a href="/profile/step2"><button type="button" class="btn btn-primary" disabled="disabled">Next >></button></a>
								</div>	   
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
	
	
	
@endsection
