@extends('layouts.app')

@section('content')

<?php 
$seekingArray = array(
	0 => "Please choose from the list",
	1 => "Man",
	2 => "Woman",
	3 => "Lesbian",
	4 => "Gay",
	5 => "Bisexual"
);

$smokeArray = array(
	0 => "Please choose from the list",
	1 => "Yes",
	2 => "No",
	3 => "Sometimes",
	4 => "Occasionaly"
);

$statArray = array(
	0 => "Please choose from the list",
	1 => "Single",
	2 => "Married",
	3 => "Engaged",
	4 => "Divorced"
);

$childArray = array(
	0 => "Please choose from the list",
	1 => "I have no",
	2 => "I have already",
	3 => "I have no and I do not want",
	4 => "I have no but I want to have"
);


$drinkArray = array(
	0 => "Please choose from the list",
	1 => "Never",
	2 => "Sometimes",
	3 => "Occasionaly",
	4 => "Yes please"
);


?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Profile - <a href="/profile/step1">Step 1</a> / Step 2 / <a href="/profile/step3">Step 3</a>
						<div class="pull-right">{{$perc}}%</div>
					</div>
                    <form id="setup2Form" class="form-horizontal" role="form" method="POST" action="{{ url('/profile/step2') }}">
                        <div class="panel-body register">

	                   

                            <div class="col-lg-6">
                                <h3>More about you</h3>
                                    {{ csrf_field() }}
								<div class="form-group row">
                                    <label for="seeking" class="col-md-4 form-control-label">Seeking</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="seeking" name="seeking">
										<?php 										
											foreach($seekingArray as $kulcs => $ertek){
												if($profile[0]->seeking == $kulcs){
													print "<option value=".$kulcs." selected>".$ertek."</option>";
												}
												else{
													print "<option value=".$kulcs.">".$ertek."</option>";
												}												
											}												
											?>
                                        </select>
                                    </div>
                                </div>
         

                                <div class="form-group row">
                                    <label for="religion" class="col-md-4 form-control-label">Religion</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="religion" name="religion" value="{{ $profile[0]->religion }}">
                                    </div>
                                </div>

                               <div class="form-group row">
                                    <label for="pets" class="col-md-4 form-control-label">Pets</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="pets" name="pets" value="{{ $profile[0]->pet }}">
                                    </div>
                                </div>

                               <div class="form-group row">
                                    <label for="smoke" class="col-md-4 form-control-label">Smoke</label>
                                    <div class="col-md-6">
									
                                        <select class="form-control" id="smoke" name="smoke">
										<?php 										
											foreach($smokeArray as $kulcs => $ertek){
												if($profile[0]->smoke == $kulcs){
													print "<option value=".$kulcs." selected>".$ertek."</option>";
												}
												else{
													print "<option value=".$kulcs.">".$ertek."</option>";
												}												
											}												
											?>
                                        </select>
                                    </div>
                                </div>								
                                    
                            </div>

                            <div class="col-lg-6">
								<h3></h3>
								<div class="form-group row">
                                    <label for="relstat" class="col-md-4 form-control-label">Relationship status</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="relationship_status" name="relationship_status">
											<?php 
											foreach($statArray as $kulcs => $ertek){
												if($profile[0]->relationship_status == $kulcs){
													print "<option value=".$kulcs." selected>".$ertek."</option>";
												}
												else{
													print "<option value=".$kulcs.">".$ertek."</option>";
												}												
											}												
											?>
                                        </select>                   
									</div>
                                </div>
         

                                <div class="form-group row">
                                    <label for="children" class="col-md-4 form-control-label">Children</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="children" name="children">
											<?php 										
											foreach($childArray as $kulcs => $ertek){
												if($profile[0]->children == $kulcs){
													print "<option value=".$kulcs." selected>".$ertek."</option>";
												}
												else{
													print "<option value=".$kulcs.">".$ertek."</option>";
												}												
											}												
											?>
                                        </select>                                    
									</div>
                                </div>
								
                                <div class="form-group row">
                                    <label for="drink" class="col-md-4 form-control-label">Drink</label>
                                    <div class="col-md-6">
                                        <select class="form-control" id="drink" name="drink">
											<?php 										
											foreach($drinkArray as $kulcs => $ertek){
												if($profile[0]->drink == $kulcs){
													print "<option value=".$kulcs." selected>".$ertek."</option>";
												}
												else{
													print "<option value=".$kulcs.">".$ertek."</option>";
												}												
											}												
											?>
                                        </select>                                    
									</div>
                                </div>		                               
                            </div>
							
							<div class="col-lg-12">
							
                                <div class="form-group row">
                                    <label for="about" class="col-md-12 form-control-label">Little about yourself</label>
                                    <div class="col-md-12">
                                        <textarea class="form-control" id="about" name="about">{{ $profile[0]->about_you }}</textarea>
									</div>
                                </div>

                                <div class="form-group row">
                                    <label for="lookingfor" class="col-md-12 form-control-label">What are you looking for?</label>
                                    <div class="col-md-12">
                                        <textarea class="form-control" id="lookingfor" name="lookingfor">{{ $profile[0]->looking_for }}</textarea>
									</div>
                                </div>

                                <div class="form-group row">
                                    <label for="hobbies" class="col-md-12 form-control-label">Hobbies & Interests</label>
                                    <div class="col-md-12">
                                        <textarea class="form-control" id="hobbies" name="hobbies">{{ $profile[0]->hobbies_interest }}</textarea>
									</div>
                                </div>								
							</div>

                            <div class="clearfix"></div>

                            <h3>Terms and Conditions</h3>
                            <div class="">                                
                                    
									
									<?php 
									if($profile[0]->termsAccepted==1){
										echo '<div>Agree <input type="checkbox" name="checkbox2" id="checkbox2" checked="checked" /></div> ';
									}
									else{
										echo '<div>Agree <input type="checkbox" name="checkbox2" id="checkbox2" /></div>';
									}
									?>  									
                                   
                            </div>
							
						  <div class="alert alert-success">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<strong>Success!</strong> <span id="sMes"></span>
							  </div>
							  
							  <div class="alert alert-danger">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<strong>Error!</strong> <span id="eMes"></span>
							</div>  
							  
								<div>
									<a href="/public/profile/step1"> <button type="button" class="btn btn-primary" id=""><< Previous </button> </a>
									<a href="/public/profile/step3" id="showNext"><button type="button" class="btn btn-primary" disabled>Next >></button></a>									
								</div>	
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
