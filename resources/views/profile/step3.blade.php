@extends('layouts.app')

@section('content')

<?php 
$goldActive = false;
$silverActive = false;
$bronzeActive = false;

if(null!==$profile[0]->package_type){
	if($profile[0]->package_type=="gold") {
		$goldActive = " checked=\"checked\"";
	}
	if($profile[0]->package_type=="silver") {
		$silverActive = " checked=\"checked\"";
	}
	if($profile[0]->package_type=="free") {
		$bronzeActive = " checked=\"checked\"";
	}	
}
?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Profile - <a href="/profile/step1">Step 1</a> / <a href="/profile/step2">Step 2</a> / Step 3
						<div class="pull-right">{{$perc}}%</div>
					</div>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/step3') }}">
                        <div class="panel-body register">
						
							  <div class="alert alert-success">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<strong>Success!</strong> <span id="sMes"></span>
							  </div>
							  
							  <div class="alert alert-danger">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<strong>Error!</strong> <span id="eMes"></span>
							  </div> 
							  
						{{ csrf_field() }}
						<h3>Select your package</h3>
								
							<div class="row">
								<div class="col-xs-4 col-sm-3 col-lg-3 block">  							
									<div></div>
									<div class="odd"> Send private message </div>
									<div> View profiles </div>
									<div class="odd"> Enchanced profile listing </div>
								</div>		
								
								<div class="col-xs-2 col-sm-3 col-lg-3 block">							
									<div class="title">Bronze</div>
									<div class="odd"></div>
									<div><img width="30" src="/img/tick.png" class="img-responsive" /></div>
									<div class="odd"></div>	
									<div>Free<br> $0.00  <br><input id="free" type="radio" name="plan" value="free" <?=$bronzeActive;?> /></div>								
								</div>
								
								<div class="col-xs-3 col-sm-3 col-lg-3 block">  							
									<div class="title">Silver</div>
									<div class="odd"><img width="30" src="/img/tick.png" class="img-responsive" /></div>
									<div><img width="30" src="/img/tick.png" class="img-responsive" /></div>
									<div class="odd"></div>	
									<div>Silver<br> $33/m <br><input id="silver" type="radio" name="plan" value="silver" <?=$silverActive;?> /></div>									
								</div>	
								
								<div class="col-xs-3 col-sm-3 col-lg-3 block">							
									<div class="title">Gold</div>
									<div class="odd"><img width="30" src="/img/tick.png" class="img-responsive" /></div>
									<div><img width="30" src="/img/tick.png" class="img-responsive" /></div>
									<div class="odd"><img width="30" src="/img/tick.png" class="img-responsive" /></div>
									<div>Gold<br> $38/m	<br><input id="gold" type="radio" name="plan" value="gold" <?=$goldActive;?> /></div>
								</div>
							</div>								

                            <div class="clearfix"></div>
                            <div class="buttons row"><a href="/profile/step2"> <button type="button" class="btn btn-primary" id=""><< Previous </button> </a>
							<a href="/"> <button type="button" class="btn btn-primary">Close</button></a></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
