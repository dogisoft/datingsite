@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Account</div>
					<form id="accountForm" autocomplete="off" class="form-horizontal" role="form" method="POST" action="{{ url('/account/update') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
								
								
							<div class="message-frame">		
								{{$activeDanger}}
								  <div class="alert alert-success fade in" <?=$activeSuccess?>>
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<strong>Success!</strong> <span id="sMes"></span>
								  </div>
								  
								  <div class="alert alert-danger fade in">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<strong>Error!</strong> <span id="eMes"><?=$message?></span>
								  </div> 									
							</div> 

                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-4 col-xs-12 control-label">Name</label>

                                        <div class="col-md-6 col-xs-12">
                                            <input maxlength="50" id="name" type="text" class="form-control" name="name" value="{{ $user->name }}">

                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 col-xs-12 control-label">E-Mail Address</label>

                                        <div class="col-md-6 col-xs-12">
                                            <input maxlength="100" id="email" type="email" class="form-control" name="email" value="{{ $user->email }}">

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 col-xs-12 control-label">Old password</label>

                                        <div class="col-md-6 col-xs-12">
                                            <input maxlength="25" id="old-password" type="password" class="form-control" name="old-password" autocomplete='off'>

                                            @if ($errors->has('old-password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('old-password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>									
									
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 col-xs-12 control-label">New password</label>

                                        <div class="col-md-6 col-xs-12">
                                            <input maxlength="25" id="password" type="password" class="form-control" name="password" autocomplete='off'>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label for="password-confirm" class="col-md-4 col-xs-12 control-label">Confirm Password</label>

                                        <div class="col-md-6 col-xs-12">
                                            <input maxlength="25" id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                            @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
											<strong>{{ $errors->first('password_confirmation') }}</strong>
										</span>
                                            @endif

                                        </div>
										
                                    </div>			
									<button type="button" class="btn btn-primary" id="">Save >></button>
					</form>
            </div>
        </div>
    </div>
</div>
@endsection
